<?php

$pages = [
    'index' => ['title' => 'Главная', 'text' => 'Это текст главной'],
    'about' => ['title' => 'О нас', 'text' => 'Это текст страницы о нас'],
    'contacts' => ['title' => 'Контакты', 'text' => 'Это текст страницы контактов']
];

$id = @$_GET['id'];
$page = $pages[$id];

return $page;


