<?php echo $this->render('header', ['title' => $content['title']]); ?>

<ul>
<?php foreach($news as $key => $item): ?>
    <li>
        <a href="<?php echo "/?controller=news&action=show&id=$key"; ?>">
            <?php echo $item['title']; ?>
        </a>
    </li>
<?php endforeach; ?>
</ul>

<?php echo $this->render('footer'); ?>
