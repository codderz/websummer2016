<?php

class XMLConfig {
    public function getXML($key){}
    public function setXML($key, $value){}
    public function export(){}
    public function import(){}
}

class Logger {

}

interface IConfig
{
    public function get($key);
    public function set($key, $value);
}

class XMLConfigAdapter implements IConfig{

    public function __construct($target)
    {
        $this->target = $target;
    }

    public function get($key)
    {
        return $this->target->getXML($key);
    }

    public function set($key, $value)
    {
        return $this->target->setXML($key, $value);
    }
}

class Config implements IConfig{
    public function get($key){}
    public function set($key, $value){}
    public function export(){}
    public function import(){}
}

class Params {

}

//

class Facade {

    public function getConfig($key){
        return $this->config->get($key);
    }

    public function setConfig($key, $value){
        return $this->config->set($key, $value);
    }

}