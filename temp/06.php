<?php

interface ICanWash {

    public function wash(ICanBeWashed $obj);

}

class Wash {

    public function wash(ICanBeWashed $obj){
        $obj->wash();
    }

}

class Base {

    public
        $logger;

    public function log($message, $data){
        $this->logger->log($message, $data);
    }


}

class Forum
{

    public function deletePost($id)
    {
        $post = new Post($id);
        $post->delete();
    }

    public function savePost($data)
    {
        $post = new Post($data);
        $post->save();
    }

}

class ConcreteForum {

    public
        $forum,
        $location;

    public function __construct(IForum $forum, ILocator $locator)
    {
        $this->forum = $forum;
        $this->locator = $locator;
    }

    public function deletePost($id)
    {
        $this->forum->deletePost($id);

        $config = $this->locator->locate('config');
        if ($config->needLog()) {
            $logger = $this->locator->locate('logger');
            $logger->log('Post deleted', $id);
        }
    }

    public function savePost($data)
    {
        $this->forum->savePost($data);

        $config = $this->locator->locate('config');
        if ($config->needLog()) {
            $logger = $this->locator->locate('logger');
            $logger->log('Post saved', $data);
        }
    }

}

$forum = new ConcreteForum(new Forum, new Locator);

class Logger
{

    public function log($message, $data)
    {

    }

}

class Config
{

    /**
     * @return bool
     */
    public function needLog()
    {

    }

}


die;

interface IReader
{

    public function read();

}

interface IWriter
{

    public function write();

}

interface IParser
{

    public function setReader(IReader $reader);

    public function setWriter(IWriter $writer);

    public function run();

}

interface IMapper
{

    public function map($data);

}

class Parser implements IParser
{

    public
        $reader,
        $writer;

    public function setReader(IReader $reader)
    {
        $this->reader = $reader;
    }

    public function setWriter(IWriter $writer)
    {
        $this->writer = $writer;
    }

    public function run()
    {
        $this->writer->write(
            $this->reader->read()
        );
    }

}

class YaParser extends Parser
{

    public function __construct()
    {
        $this->setReader(new UrlReader('http://news.yandex.ru', new YandexParser));
        $this->setWriter(new MySQLWriter);
    }

}

class NewsRuParser extends Parser
{

    public function __construct()
    {
        $this->setReader(new UrlReader('http://news.ru', new NewsRuParser));
        $this->setWriter(new MySQLWriter);
    }

}

$parser = new NewsRuParser();
$parser->run();
