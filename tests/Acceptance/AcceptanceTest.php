<?php

namespace Acceptance\Tests;

use Tests\Selenium2TestCase;

/**
 * @author eshigaev
 */
class AcceptanceTest extends Selenium2TestCase
{

    public function setUp()
    {
        //java -jar /var/www/selenium-server-standalone-2.53.0.jar -Dwebdriver.chrome.driver=/var/www/chromedriver
        $this->setBrowser('chrome');
        $this->setBrowserUrl('https://www.google.ru/');
        parent::setUp();
    }

    public function test()
    {
        $this->timeouts()->implicitWait(5000);

        $this->url('/');

        $input = $this->byName('q');
        $input->value('Codderz');
        $this->byName('btnG')->click();

        $element = $this->byCssSelector('#ires');
        $this->assertContains('Codderz - Web App Development', $element->text());
    }

}