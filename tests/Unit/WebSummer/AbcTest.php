<?php

namespace Tests\Unit\WebSummer;

use Tests\TestCase;
use WebSummer\Abc;

class AbcTest extends TestCase
{

    public function testMake(){
        $target = new Abc();
        $this->assertInstanceOf(Abc::class, $target);
    }

}