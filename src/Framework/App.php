<?php

namespace Framework;

class App
{

    public function run($controller, $action){
        //параметры
        if (!$controller){
            $controller = 'pages';
        }
        if (!$action){
            $action = 'index';
        }

        //создаем
        $controller = 'App\\Controllers\\' . ucfirst($controller) . 'Controller';
        $controller = new $controller();
        return $controller->$action();
    }

}