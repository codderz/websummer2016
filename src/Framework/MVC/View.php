<?php

namespace Framework\MVC;

class View
{

    public function render($template, $params = []){
        ob_start();

        $fileName = '../templates/' . $template . '.php';
        if (!file_exists($fileName)){
            return null;
        }

        extract($params);
        require $fileName;

        $content = ob_get_contents();
        ob_clean();

        return $content;
    }

}