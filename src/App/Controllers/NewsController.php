<?php


namespace App\Controllers;

use Framework\MVC\Controller;
use App\Models\NewsModel;

class NewsController extends Controller
{

    protected
        $model;

    public function __construct()
    {
        $this->model = new NewsModel();
        parent::__construct();
    }

    public function index(){
        $news = $this->model->all();
        return $this->view->render('news', [
            'content' => [
                'title' => 'Список новостей'
            ],
            'news' => $news
        ]);
    }

    public function show(){
        $id = $_GET['id'];

        $item = $this->model->findById($id);
        return $this->view->render('news-item', [
            'content' => [
                'title' => 'Новость целиком'
            ],
            'item' => $item
        ]);
    }

}