<?php

namespace App\Controllers;

use Framework\MVC\Controller;
use App\Models\PagesModel;

class PagesController extends Controller
{

    protected
        $model;

    public function __construct()
    {
        $this->model = new PagesModel();
        parent::__construct();
    }

    public function index(){
        $id = isset($_GET['id']) ? $_GET['id'] : 'index';

        $content = $this->model->findById($id);
        return $this->view->render('pages', [
            'content' => $content
        ]);
    }

}