<?php

namespace App\Models;

class PagesModel
{

    public function all(){
        return [
            'index' => ['title' => 'Главная', 'text' => 'Это текст главной'],
            'about' => ['title' => 'О нас', 'text' => 'Это текст страницы о нас'],
            'contacts' => ['title' => 'Контакты', 'text' => 'Это текст страницы контактов']
        ];
    }

    public function findById($id){
        return $this->all()[$id];
    }

}