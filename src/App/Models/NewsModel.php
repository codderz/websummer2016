<?php

namespace App\Models;

class NewsModel
{

    public function all(){
        return [
            1 => ['title' => 'Первая', 'text' => 'Это текст первой новости'],
            2 => ['title' => 'Вторая', 'text' => 'Это текст второй новости'],
            3 => ['title' => 'Третья', 'text' => 'Это текст третьей новости']
        ];
    }

    public function findById($id){
        return $this->all()[$id];
    }

}